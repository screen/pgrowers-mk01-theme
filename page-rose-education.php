<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start justify-content-center">
                    <div class="glossary-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                        <h2 class="glossary-section-title">
                            <?php _e('Glossary', 'pgrowers'); ?>
                        </h2>

                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="glossary-terms col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <?php for ($i = 65 ; $i<=90; $i++) { ?>
                                    <a onclick="filter_glossary('<?php echo chr($i); ?>')" class="alphabet">
                                        <?php echo chr($i); ?></a>
                                    <?php } ?>
                                </div>
                                <div class="w-100"></div>
                            </div>
                        </div>

                        <div class="container">
                            <div class="row">

                                <?php $args = array('post_type' => 'glossary', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'name'); ?>
                                <?php $glossary_array = new WP_Query($args); ?>
                                <?php if ($glossary_array->have_posts()) : ?>
                                <div class="table-responsive">
                                    <table class="table table-hover table-glossary">
                                        <thead>
                                            <tr>
                                                <th scope="col">Word</th>
                                                <th scope="col">Meaning</th>
                                            </tr>
                                        </thead>
                                        <tbody class="glossary_results">
                                            <?php while ($glossary_array->have_posts()) : $glossary_array->the_post(); ?>
                                            <tr>
                                                <td colspan="2">
                                                    <h3>
                                                        <?php the_title(); ?>
                                                    </h3>
                                                    <?php the_content(); ?>
                                                </td>
                                            </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                        <div class="glossary-pagination col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
