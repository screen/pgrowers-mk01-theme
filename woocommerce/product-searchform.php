<?php
/**
 * The template for displaying product search form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<form role="search" method="get" id="searchform" class="searchform woocommerce-product-search" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <input type="search" class="form-control" name="s" id="s" aria-describedby="button-addon2" value="" placeholder="<?php _e('Search', 'pgrowers'); ?>">
        <input type="hidden" name="post_type" value="product" />
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
