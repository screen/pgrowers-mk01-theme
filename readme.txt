![alt tag](http://robertochoa.com.ve/wp-content/uploads/2015/10/repo-logo.jpg)

# pgrowers Wordpress Custom Theme #

* Version: 1.0
* Design: [Robert Ochoa](http://www.robertochoa.com.ve/)
* Development: [Robert Ochoa](http://www.robertochoa.com.ve/)

Tema diseñado por [Robert Ochoa](http://www.robertochoa.com.ve/) para pgrowers Este tema custom fue construido en su totalidad, pasando por su etapa de Wireframing, rearmado, version anterior e implementación en hosting externo.

### Componentes Principales ###

* Twitter Bootstrap 3.3.7
* Animate 3.5.2
* Desandro's Flickity 2.0.10
* OwlCarousel 2.2.1
* Font Awesome 4.7.0
* jQuery Nicescroll 3.7.6
* modernizr 2.8.3
* jQuery Sticky 1.0.4
* Lettering 0.7.0
* Smooth Scroll 12.1.5
* Desandro's Isotope 3.0.4
* Desandro's Masonry 4.2.0

### Funciones Incluídas ###

* Custom Post Type:
* Custom Taxonomies:
* Wordpress Menu Structure
* Custom Metabox:
* Custom contact section.

### Plugins Requeridos ###

* Rilwis' Metabox
* Jetpack by Wordpress

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: [Robert Ochoa](http://www.robertochoa.com.ve/)

Main Developer: [Robert Ochoa](http://www.robertochoa.com.ve/)
