<?php get_header(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="page-title-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h1 class="text-center"><?php single_cat_title(); ?></h1>
        </div>
        <section class="page-section col-12">
            <?php if (have_posts()): ?>
            <div class="row">
                <div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="container">
                        <div class="row">
                            <div class="archive-container col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                                <div class="row">
                                    <?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
                                    <?php $i = 1; ?>
                                    <?php while (have_posts()) : the_post(); ?>
                                    <article id="post-<?php the_ID(); ?>" class="archive-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                                        <div class="archive-item-wrapper">
                                            <div class="container p-0">
                                                <div class="row no-gutters">
                                                    <picture class="archive-item-picture col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" >
                                                        <?php if ( has_post_thumbnail()) : ?>
                                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                            <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                                        </a>
                                                        <?php else : ?>
                                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                            <img itemprop="image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-fluid" />
                                                        </a>
                                                        <?php endif; ?>
                                                        <div class="archive-item-date" datetime="<?php echo get_the_time('Y-m-d') ?>" itemprop="datePublished"><div><?php the_time('M'); ?></div><div><?php the_time('d'); ?></div></div>
                                                    </picture>
                                                    <div class="archive-item-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <header>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                                                            </a>
                                                        </header>
                                                        <div class="category-item-info">
                                                            <?php $terms = get_the_category(); ?>
                                                            <?php foreach ($terms as $term) { ?>
                                                            <a href="<?php echo esc_url(get_category_link($term));?>"><?php echo $term->name; ?></a>
                                                            <?php } ?>
                                                        </div>
                                                        <p><?php the_excerpt(); ?></p>
                                                        <a href="<?php the_permalink(); ?>" title="<?php _e('See More', 'pgrowers');?>" class="archive-item-readmore"><?php _e('Read More', 'pgrowers'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <?php $i++; endwhile; ?>
                                    <div class="pagination col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-sidebar float-right col-xl-3 col-lg-3 col-md-4 d-xl-flex d-lg-flex d-md-none d-sm-none d-none">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php else: ?>
            <article class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h2><?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'pgrowers'); ?></h2>
                <h3><?php _e('Dirígete nuevamente al', 'pgrowers'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'pgrowers'); ?>"><?php _e('inicio', 'pgrowers'); ?></a>.</h3>
            </article>
            <?php endif; ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
