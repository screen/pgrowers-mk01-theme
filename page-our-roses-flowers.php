<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </div>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php $about_people_images = get_custom_metabox_group(get_the_ID(), 'pg_rose_flowers_filter_group'); ?>

                    <?php if (!empty($about_people_images)) { ?>
                    <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                    <div class="product-filter-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="product-filter-item-wrapper">
                            <div class="product-filter-item-img">
                                <?php echo wp_get_attachment_image( $feature['pg_rose_flowers_filter_image_id'], 'vertital_img', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <div class="product-filter-item-content">
                                <?php echo apply_filters('the_content', $feature['pg_rose_flowers_filter_content']); ?>
                                <a class="btn btn-md btn-filter" href="<?php echo esc_url($feature['pg_rose_flowers_filter_url']); ?>">
                                    <?php _e('View More'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="page-section page-slider-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="text-center">
                            <?php _e('Our Most Popular Roses & Flowers'); ?>
                        </h2>
                    </div>
                    <div class="product-slider-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div id="product_slider" class="product-slider owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php $query_news = new WP_Query($args); ?>
                            <?php if ($query_news->have_posts()) : ?>
                            <?php while ($query_news->have_posts()) : $query_news->the_post(); ?>
                            <div class="product-slider-item item">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                                </a>
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php wp_reset_query(); ?>
        </section>
        <?php
        $post = get_page_by_path( 'home' );
        setup_postdata( $post );
        get_template_part('templates/home-block-rose-special');
        wp_reset_query();
        ?>


    </div>
</main>
<?php get_footer(); ?>
