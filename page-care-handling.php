<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="page-img-container col-xl-6 col-lg-6 col-md-6  d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                    </div>
                    <div class="page-text-container rose-color-guide-main-desc col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="w-100"></div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $content = get_post_meta(get_the_ID(), 'pg_care_handling_quote', true); ?>
                <?php echo apply_filters('the_content', $content); ?>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
