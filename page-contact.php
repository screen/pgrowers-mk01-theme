<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="contact-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="contact-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php get_template_part('templates/block-contact-form'); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">

                    <?php $count_delay = 50; ?>
                    <?php $contact_items_array = get_custom_metabox_group(get_the_ID(), 'pg_contact_page_group'); ?>
                    <?php foreach ( $contact_items_array as $feature ) {?>
                    <div class="contact-info-item col-xl col-lg col-md col-sm-12 col-12" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <div class="contact-info-item-image">
                            <?php echo wp_get_attachment_image( $feature['pg_contact_item_image_id'], 'full', false, array('class' => 'img-fluid') ); ?>
                        </div>
                        <div class="contact-info-item-content">
                            <?php echo apply_filters( 'the_content', $feature['pg_contact_item_content'] ); ?>
                        </div>
                    </div>
                    <?php $count_delay = $count_delay + 50; ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
