jQuery(document).ready(function ($) {
    "use strict";
    //    jQuery("#sticker").sticky({
    //        topSpacing: 0
    //    });
    //    jQuery("body").niceScroll();

    var lastScrollTop = 0;
    jQuery(window).on("scroll", function () {
        var st = jQuery(this).scrollTop();
        st > lastScrollTop ? jQuery(".floating-nav").addClass("is-hidden") : jQuery(window).scrollTop() > 200 ? (jQuery(".floating-nav").removeClass("is-hidden"), setTimeout(function () {}, 200)) : jQuery(".floating-nav").addClass("is-hidden"), lastScrollTop = st, 0 == jQuery(this).scrollTop() && jQuery(".floating-nav").addClass("is-hidden");
    });

    jQuery('.slider-container').owlCarousel({
        items: 1,
        loop: true,
        navs: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });

    jQuery('.page-section-slider').owlCarousel({
        items: 3,
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            }
        },
        loop: true,
        nav: true,
        dots: false,
        margin: 20,

    });

    jQuery('.page-img-overlayed-slider').owlCarousel({
        items: 2,
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            768: {
                items: 2
            }
        },
        loop: true,
        nav: true,
        dots: false,
        margin: 20,

    });

    jQuery('#product_slider').owlCarousel({
        items: 4,
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            768: {
                items: 2
            },
            991: {
                items: 3
            }
        },
        loop: true,
        nav: true,
        dots: false,
        margin: 20,
    });

    setTimeout(function () {
        AOS.init();
    }, 1000);


}); /* end of as page load scripts */

function filter_glossary(FrstLetter) {
    jQuery.ajax({
        type: 'POST',
        url: ajax_object.ajaxurl,
        data: {
            firstletter: FrstLetter,
            action: 'pgrowers_filter_glossary'
        },
        beforeSend: function() {
            jQuery('.glossary_results').html('<tr><td colspan="2"><div class="loader"><div class="ball-clip-rotate-multiple"><div></div><div></div></div></div></td></tr>');
        },
        success: function (data) {
            jQuery('.glossary_results').html(data);
        }
    });
}
