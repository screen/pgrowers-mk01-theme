<?php
/* --------------------------------------------------------------
/* PROFILE ABOUT METABOX
-------------------------------------------------------------- */
$cmb_contact_page = new_cmb2_box( array(
    'id'            => $prefix . 'contact_page_metabox',
    'title'         => __( 'Sección: Items de Contacto', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'contact', 'alt_value' => 'page-contact.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_contact_page->add_field( [
    'id'      => $prefix . 'contact_page_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Item {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Item', 'pgrowers' ),
        'remove_button' => __( 'Remover Item', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_contact_page->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Item', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'contact_item_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_contact_page->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'contact_item_content',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );
