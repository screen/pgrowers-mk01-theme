<?php
/* --------------------------------------------------------------
/* COMMON FUNCTIONS
-------------------------------------------------------------- */

/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

/* ADD CONTENT WIDTH FUNCTION */
if ( ! isset( $content_width ) ) $content_width = 1170;


/* --------------------------------------------------------------
/* CUSTOM WIDGETS - CUSTOM MAILCHIMP FORM
-------------------------------------------------------------- */

class pgrowers_custom_mailchimp_form extends WP_Widget {

    public function pgrowers_custom_mailchimp_form_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_media();
        wp_enqueue_script('admin-functions', get_template_directory_uri() . '/js/admin-functions.js', array('jquery'), null, true);
    }

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_mailchimp_form',
            'description' => __('Este widget custom mostrará un formulario de Mailchimp personalizado, deberá agregarse la lista de Mailchimp al cual el user ingresará al momento de registrarse', 'pgrowers')
        );
        parent::__construct( 'pgrowers_custom_mailchimp_form', 'Custom Mailchimp Form', $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'pgrowers_custom_mailchimp_form_scripts'));
    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
        $unique_formid = uniqid('mailchimp_form_');
        $unique_id = substr($unique_formid, 15);
        $unique_buttonid = 'mailchimp_button_' . substr($unique_formid, 15);
?>
<form id="<?php echo $unique_formid; ?>" class="mailchimp_form" type="POST">
    <?php
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
    ?>
    <i class="fa fa-heart-o"></i>
    <div class="custom-form-text-container">
        <?php
        if ( ! empty( $instance['extra_text'] ) ) {
            echo apply_filters( 'the_content', $instance['extra_text'] );
        }
        ?>
    </div>
    <div class="custom-form-control-container">
        <input type="text" name="FNAME" id="FNAME" class="form-control" placeholder="<?php echo esc_html_e('Name', 'pgrowers'); ?>" />
        <small class="danger d-none"></small>
        <input type="email" name="EMAIL" id="EMAIL" class="form-control" placeholder="<?php echo esc_html_e('Email Address', 'pgrowers'); ?>" />
        <small class="danger d-none"></small>
        <input type="hidden" name="mailchimp-list" id="mailchimp-list" class="form-control" value="<?php echo $mailchimp_list; ?>" />
        <input type="hidden" name="file_download" id="file_download" class="form-control" value="<?php echo $file_download; ?>" />
    </div>
    <button id="<?php echo $unique_buttonid; ?>" class="btn btn-md btn-submit">
        <?php _e('Submit', 'pgrowers'); ?></button>
    <div id="response_<?php echo $unique_id; ?>" class="form-response"></div>
</form>

<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $extra_text = ! empty( $instance['extra_text'] ) ? $instance['extra_text'] : '';
        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <small>
        <?php _e('Título del Widget', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'extra_text' ) ); ?>">
        <?php esc_attr_e( 'Texto descriptivo:', 'pgrowers' ); ?></label>
    <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'extra_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extra_text' ) ); ?>" type="text"><?php echo esc_attr( $extra_text ); ?></textarea>
    <small>
        <?php _e('Aquí podrá agregar un texto descriptivo corto', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'list' ) ); ?>">
        <?php esc_attr_e( 'Lista de Mailchimp:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'mailchimp_list' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mailchimp_list' ) ); ?>" type="text" value="<?php echo esc_attr( $mailchimp_list ); ?>">
    <small>
        <?php _e('Aquí podrá agregar el ListID de Mailchimp a donde debe llegar el correo del suscriptor', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'file_download' ) ); ?>">
        <?php esc_attr_e( 'Archivo a descargar al suscribirse: (Opcional)', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'file_download' ); ?>" name="<?php echo $this->get_field_name( 'file_download' ); ?>" value="<?php echo esc_attr( $file_download ); ?>" />
    <small>
        <?php _e('Opcional: Agregue acá el archivo que el suscriptor ha ganado por suscribirse a Mailchimp', 'pgrowers'); ?></small>

    <button class="upload_image_button button button-primary" style="display: block; margin-top: .3rem;">
        <?php _e('Cargar Archivo', 'pgrowers'); ?></button>
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['extra_text'] = ( ! empty( $new_instance['extra_text'] ) ) ? sanitize_text_field( $new_instance['extra_text'] ) : '';
        $instance['mailchimp_list'] = ( ! empty( $new_instance['mailchimp_list'] ) ) ? sanitize_text_field( $new_instance['mailchimp_list'] ) : '';
        $instance['file_download'] = ( ! empty( $new_instance['file_download'] ) ) ? esc_url_raw( $new_instance['file_download'] ) : '';
        return $instance;
    }
}


/* --------------------------------------------------------------
/* CUSTOM WIDGETS - CUSTOM SIDEBAR BANNER
-------------------------------------------------------------- */

class pgrowers_custom_sidebar_banner extends WP_Widget {

    public function pgrowers_custom_sidebar_banner_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_media();
        wp_enqueue_script('admin-functions', get_template_directory_uri() . '/js/admin-functions.js', array('jquery'), null, true);
    }

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_sidebar_banner',
            'description' => __('Este widget custom mostrará un banner con todos los datos especificados dentro del mismo.', 'pgrowers')
        );
        parent::__construct( 'pgrowers_custom_sidebar_banner', 'Custom Banner Sidebar', $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'pgrowers_custom_sidebar_banner_scripts'));
    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        $image_banner = ! empty( $instance['image_banner'] ) ? $instance['image_banner'] : '';
        $desc_text = ! empty( $instance['desc_text'] ) ? $instance['desc_text'] : '';
        $button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : '';
        $button_url = ! empty( $instance['button_url'] ) ? $instance['button_url'] : '';
?>
<aside class="custom-banner-sidebar-container">
    <picture class="custom-banner-sidebar-image-container">
        <?php if ( ! empty( $image_banner ) ) { ?>
        <img src="<?php echo esc_url($image_banner); ?>" alt="<?php echo $desc_text; ?>" class="img-fluid img-sidebar-banner" />
        <?php } ?>
    </picture>
    <div class="custom-banner-sidebar-info-container">
        <?php
        if ( ! empty( $desc_text ) ) {
            echo apply_filters( 'the_content', $desc_text );
        }
        ?>
        <?php if ( ! empty( $button_text ) ) { ?>
        <a href="<?php echo esc_url($button_url); ?>" title="<?php echo $button_text; ?>" class="btn btn-md btn-sidebar">
            <?php echo $button_text; ?></a>
        <?php } ?>
    </div>
</aside>
<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $image_banner = ! empty( $instance['image_banner'] ) ? $instance['image_banner'] : '';
        $desc_text = ! empty( $instance['desc_text'] ) ? $instance['desc_text'] : '';
        $button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : '';
        $button_url = ! empty( $instance['button_url'] ) ? $instance['button_url'] : '';

?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'image_banner' ) ); ?>">
        <?php esc_attr_e( 'Image Banner', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'image_banner' ); ?>" name="<?php echo $this->get_field_name( 'image_banner' ); ?>" value="<?php echo esc_attr( $image_banner ); ?>" />
    <small>
        <?php _e('Add here the image for this sidebar banner', 'pgrowers'); ?></small>

    <button class="upload_image_button button button-primary" style="display: block; margin-top: .3rem;">
        <?php _e('Upload Image', 'pgrowers'); ?></button>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'desc_text' ) ); ?>">
        <?php esc_attr_e( 'Banner Text:', 'pgrowers' ); ?></label>
    <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'desc_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'desc_text' ) ); ?>" type="text"><?php echo esc_attr( $desc_text ); ?></textarea>
    <small>
        <?php _e('Add here a short text, describing the banner/promo', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>">
        <?php esc_attr_e( 'Button Text:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_text' ) ); ?>" type="text" value="<?php echo esc_attr( $button_text ); ?>">
    <small>
        <?php _e('Enter a text describing the action of this banner.', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'button_url' ) ); ?>">
        <?php esc_attr_e( 'Button URL:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'button_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_url' ) ); ?>" type="text" value="<?php echo esc_attr( $button_url ); ?>">
    <small>
        <?php _e('Enter a link with the action of this banner.', 'pgrowers'); ?></small>
</p>


<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['image_banner'] = ! empty( $new_instance['image_banner'] ) ? esc_url_raw($new_instance['image_banner']) : '';
        $instance['desc_text'] = ! empty( $new_instance['desc_text'] ) ? sanitize_text_field($new_instance['desc_text']) : '';
        $instance['button_text'] = ! empty( $new_instance['button_text'] ) ? sanitize_text_field($new_instance['button_text']) : '';
        $instance['button_url']  = ! empty( $new_instance['button_url'] ) ? sanitize_text_field($new_instance['button_url']) : '';
        return $instance;
    }
}

/* --------------------------------------------------------------
/* CUSTOM WIDGETS - CUSTOM CONTACT FORM
-------------------------------------------------------------- */

class pgrowers_custom_contact_form extends WP_Widget {


    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_contact_form',
            'description' => __('Este widget custom mostrará un formulario personalizado', 'pgrowers')
        );
        parent::__construct( 'pgrowers_custom_contact_form', 'Custom Contact Form', $widget_ops );

    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        $unique_formid = uniqid('contact_form_');
        $unique_id = substr($unique_formid, 15);
        $unique_buttonid = 'contact_form_' . substr($unique_formid, 15);
?>
<form id="<?php echo $unique_formid; ?>" class="custom_contact_form_container" type="POST">
    <?php
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
    ?>
    <div class="custom-form-text-container">
        <?php
        if ( ! empty( $instance['extra_text'] ) ) {
            echo apply_filters( 'the_content', $instance['extra_text'] );
        }
        ?>
    </div>
    <div class="custom-form-control-container">
        <input type="email" name="email" id="email" class="form-control" placeholder="<?php echo esc_html_e('Your Email Address', 'pgrowers'); ?>" />
        <small class="danger d-none"></small>
        <textarea name="comments" id="comments" cols="30" rows="4" class="form-control" placeholder="<?php echo esc_html_e('Your Comments', 'pgrowers'); ?>" ></textarea>
        <small class="danger d-none"></small>
    </div>
    <button id="<?php echo $unique_buttonid; ?>" class="btn btn-md btn-submit">
        <?php _e('Submit', 'pgrowers'); ?></button>
    <div id="response_<?php echo $unique_id; ?>" class="form-response"></div>
</form>

<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $extra_text = ! empty( $instance['extra_text'] ) ? $instance['extra_text'] : '';
?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <small>
        <?php _e('Título del Widget', 'pgrowers'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'extra_text' ) ); ?>">
        <?php esc_attr_e( 'Texto descriptivo:', 'pgrowers' ); ?></label>
    <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'extra_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extra_text' ) ); ?>" type="text"><?php echo esc_attr( $extra_text ); ?></textarea>
    <small>
        <?php _e('Aquí podrá agregar un texto descriptivo corto', 'pgrowers'); ?></small>
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['extra_text'] = ( ! empty( $new_instance['extra_text'] ) ) ? sanitize_text_field( $new_instance['extra_text'] ) : '';
        return $instance;
    }
}

/* --------------------------------------------------------------
/* CUSTOM WIDGETS - CUSTOM CONTACT FORM
-------------------------------------------------------------- */

class pgrowers_custom_woocommerce_color_filter extends WP_Widget {

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_woocommerce_color_filter',
            'description' => __('Este widget custom mostrará un filtro de colores personalizado', 'pgrowers')
        );
        parent::__construct( 'pgrowers_custom_woocommerce_color_filter', 'Custom Woocommerce Color Filter', $widget_ops );



    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
?>
<div class="custom-woocommerce-filter-color-container">
    <?php
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
    ?>
    <?php ?>
    <?php $terms = get_terms(array('taxonomy' => 'pa_color', 'hide_empty' => 'true')); ?>
    <div class="custom-woocommerce-filter-boxes">
        <?php $pagename = get_query_var('pagename');
        if ( !$pagename && $id > 0 ) {
            // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
            $post = $wp_query->get_queried_object();
            $pagename = $post->post_name;
        } ?>
        <?php $queried_color = $_GET['filter_color']; ?>
        <?php foreach ($terms as $term) {?>
        <?php if ($term->slug === $queried_color) { $active = 'color-active'; } else { $active = ''; } ?>
        <a href="<?php echo $pagename . '?filter_color=' .$term->slug; ?>" class="box-item <?php echo $active; ?>" style="background-color: #<?php echo get_term_meta($term->term_id, 'pa_color_custom_color', true); ?>"></a>

        <?php } ?>
    </div>
</div>

<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'pgrowers' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <small>
        <?php _e('Filtro por Color', 'pgrowers'); ?></small>
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        return $instance;
    }
}
