<?php

/* GET SHOP CATEGORY */
//function get_color_rose_guide() {
//    $shop_category = array();
//    $return_categories = array();
//    $shop_category = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false));
//    if ( ! empty( $shop_category ) && ! is_wp_error( $shop_category ) ){
//        foreach ( $shop_category as $shop_cat_item ) {
//            $return_categories[$shop_cat_item->term_id] = $shop_cat_item->name;
//        }
//    }
//
//    return $return_categories;
//}

/* GET SHOP CATEGORY */

/* --------------------------------------------------------------
/* HOME METABOX FUNCTIONS
-------------------------------------------------------------- */
/* --------------------------------------------------------------
/* SLIDER METABOX
-------------------------------------------------------------- */
$cmb_color_guide_slider = new_cmb2_box( array(
    'id'            => $prefix . 'color_guide_desc_metabox',
    'title'         => __( 'Sección: Descripción', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'rose-color-guide', 'alt_value' => 'page-rose-color-guide.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_color_guide_quote = new_cmb2_box( array(
    'id'            => $prefix . 'color_guide_quote_metabox',
    'title'         => __( 'Sección: Cita', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'rose-color-guide', 'alt_value' => 'page-rose-color-guide.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_color_guide_quote->add_field( array(
    'name' => __( 'Contenido', 'cmb2' ),
    'desc' => __( 'Ingrese el contenido de esta sección', 'cmb2' ),
    'id'   => $prefix . 'color_guide_quote',
    'type' => 'wysiwyg'
) );


/* --------------------------------------------------------------
ROSE COLOR GUIDE OPTIONS
-------------------------------------------------------------- */

$cmb_color_guide = new_cmb2_box( array(
    'id'            => $prefix . 'rose_color_features',
    'title'         => esc_html__( 'Rose Color Features', 'cmb2' ),
    'object_types'  => array( 'color-guide' ), // Post type
) );

$cmb_color_guide->add_field( array(
    'name' => esc_html__( 'Color Icon', 'cmb2' ),
    'desc' => esc_html__( 'Enter color icon', 'cmb2' ),
    'id'   => $prefix . 'guide_color_icon',
    'type' => 'file',
    'preview_size' => [ 100, 100 ]
) );



/* --------------------------------------------------------------
ROSE COLOR GUIDE OPTIONS
-------------------------------------------------------------- */

$cmb_color_guide = new_cmb2_box( array(
    'id'            => $prefix . 'rose_color_features',
    'title'         => esc_html__( 'Rose Color Features', 'cmb2' ),
    'object_types'  => array( 'color-guide' ), // Post type
) );

$cmb_color_guide->add_field( array(
    'name' => esc_html__( 'Color Icon', 'cmb2' ),
    'desc' => esc_html__( 'Enter color icon', 'cmb2' ),
    'id'   => $prefix . 'guide_color_icon',
    'type' => 'colorpicker',
    'default' => '#ffffff',
     'options' => array(
         'alpha' => false, // Make this a rgba color picker.
     )
) );

/* --------------------------------------------------------------
/* CARE AND HANDLING METABOX
-------------------------------------------------------------- */
$cmb_care_handling_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'care_handling_metabox',
    'title'         => __( 'Sección: Quote', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'care-handling', 'alt_value' => 'page-care-handling.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );


$cmb_care_handling_metabox->add_field( array(
    'name' => __( 'Contenido', 'cmb2' ),
    'desc' => __( 'Ingrese el contenido de esta sección', 'cmb2' ),
    'id'   => $prefix . 'care_handling_quote',
    'type' => 'wysiwyg'
) );
