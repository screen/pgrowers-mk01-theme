<?php

/* --------------------------------------------------------------
/* GIVING BACK METABOX FUNCTIONS
-------------------------------------------------------------- */
/* --------------------------------------------------------------
/* SECTION 1
-------------------------------------------------------------- */
$cmb_giving_section1 = new_cmb2_box( array(
    'id'            => $prefix . 'giving_section1_metabox',
    'title'         => __( 'Sección: 1', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'giving-back', 'alt_value' => 'page-giving-back.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_giving_section1->add_field( [
    'id'      => $prefix . 'giving_section1_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Imagen {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Imagen', 'pgrowers' ),
        'remove_button' => __( 'Remover Imagen', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_giving_section1->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'giving_image_scalated',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );


/* --------------------------------------------------------------
/* SECTION 2 METABOX
-------------------------------------------------------------- */
$cmb_giving_section2 = new_cmb2_box( array(
    'id'            => $prefix . 'giving_section2_metabox',
    'title'         => __( 'Sección: 2', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'giving-back', 'alt_value' => 'page-giving-back.php' ),

    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_giving_section2->add_field( array(
    'name' => __( 'Título de Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el título principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'giving_section2_title',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );


$group_field_id = $cmb_giving_section2->add_field( [
    'id'      => $prefix . 'giving_section2_double_desc_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Descripción {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Descripción', 'pgrowers' ),
        'remove_button' => __( 'Remover Descripción', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_giving_section2->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'giving_section2_desc',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

/* --------------------------------------------------------------
/* SECTION 3 SLIDER
-------------------------------------------------------------- */

$cmb_giving_section3 = new_cmb2_box( array(
    'id'            => $prefix . 'giving_section3_metabox',
    'title'         => __( 'Sección: 3', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'giving-back', 'alt_value' => 'page-giving-back.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_giving_section3->add_field( [
    'id'      => $prefix . 'giving_section3_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Imagen {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Imagen', 'pgrowers' ),
        'remove_button' => __( 'Remover Imagen', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_giving_section3->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'giving_image_scalated',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );
