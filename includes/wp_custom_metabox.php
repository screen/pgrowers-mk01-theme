<?php

function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );


function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

function get_custom_metabox_group($id, $post_meta) {
    $features_group = (array) get_post_meta( $id, $post_meta, true );
    foreach ($features_group as $key => $value) {
        if (empty($value)) {
            unset($features_group[$key]);
        }
    }
    return $features_group;
}

add_action( 'cmb2_admin_init', 'pgrowers_register_demo_metabox' );

function pgrowers_register_demo_metabox() {
    $prefix = 'pg_';


    /* --------------------------------------------------------------
PAGES OPTIONS
-------------------------------------------------------------- */

    $cmb_page_general = new_cmb2_box( array(
        'id'            => $prefix . 'page_general_metabox',
        'title'         => __( 'Metabox General', 'cmb2' ),
        'object_types'  => array( 'page' ), // Post typea
        'context'       => 'side',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'closed'     => true, // true to keep the metabox closed by default
        'classes'    => 'extra-class', // Extra cmb2-wrap classes
        'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
    ) );

    $cmb_page_general->add_field( array(
        'name' => __( 'Banner de Sección', 'cmb2' ),
        'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
        'id'   => $prefix . 'page_general_banner',
        'type' => 'file',
        'preview_size' => [ 300, 300 ]
    ) );



    /*
--------------------------------------------------------------
/* HOME METABOX FUNCTIONS
-------------------------------------------------------------- */

    require_once('custom-home-metaboxes.php');

    /*
--------------------------------------------------------------
/* ROSE COLOR GUIDE SPECIAL METABOX
-------------------------------------------------------------- */

    require_once('custom-rose-guide-metaboxes.php');

    /*
--------------------------------------------------------------
/* ABOUT PAGES SPECIAL METABOX
-------------------------------------------------------------- */

    require_once('custom-about-pages-metaboxes.php');


    /* -------------------------------------------------------------
/* ROSE FLOWERS PAGES SPECIAL METABOX
-------------------------------------------------------------- */

    require_once('custom-roses-flowers-metaboxes.php');

    /* -------------------------------------------------------------
/* ROSE FLOWERS PAGES SPECIAL METABOX
-------------------------------------------------------------- */

    require_once('custom-giving-back-metaboxes.php');

       /* -------------------------------------------------------------
/* CONTACT US PAGE SPECIAL METABOX
-------------------------------------------------------------- */

    require_once('custom-contact-page-metaboxes.php');

    /* --------------------------------------------------------------
PRODUCT OPTIONS
-------------------------------------------------------------- */

    $cmb_product = new_cmb2_box( array(
        'id'            => $prefix . 'product_metabox',
        'title'         => esc_html__( 'Fact Sheet', 'pgrowers' ),
        'object_types'  => array( 'product' ), // Post type
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Vase Life', 'pgrowers' ),
        'id'      => $prefix . 'product_vase_life',
        'type'    => 'radio_inline',
        'options' => array(
            'short' => __( 'Short', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'long'     => __( 'Long', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Bloom Size', 'pgrowers' ),
        'id'      => $prefix . 'product_bloom_size',
        'type'    => 'radio_inline',
        'options' => array(
            'small' => __( 'Small', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'large'     => __( 'Large', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Aperture', 'pgrowers' ),
        'id'      => $prefix . 'product_aperture',
        'type'    => 'radio_inline',
        'options' => array(
            'small' => __( 'Small', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'large'     => __( 'Large', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Aperture Speed', 'pgrowers' ),
        'id'      => $prefix . 'product_aperture_speed',
        'type'    => 'radio_inline',
        'options' => array(
            'slow' => __( 'Slow', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'fast'     => __( 'Fast', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Fragance', 'pgrowers' ),
        'id'      => $prefix . 'product_fragance',
        'type'    => 'radio_inline',
        'options' => array(
            'slight' => __( 'Slight', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'strong'     => __( 'Strong', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Petal Count', 'pgrowers' ),
        'id'      => $prefix . 'product_petal_count',
        'type'    => 'radio_inline',
        'options' => array(
            'few' => __( 'Few', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'many'     => __( 'Many', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Foliage', 'pgrowers' ),
        'id'      => $prefix . 'product_floiage',
        'type'    => 'radio_inline',
        'options' => array(
            'light' => __( 'Light', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'heavy'     => __( 'Heavy', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );

    $cmb_product->add_field( array(
        'name'    => esc_html__( 'Thron Count', 'pgrowers' ),
        'id'      => $prefix . 'product_thorn_count',
        'type'    => 'radio_inline',
        'options' => array(
            'few' => __( 'Few', 'pgrowers' ),
            'medium'   => __( 'Medium', 'pgrowers' ),
            'many'     => __( 'Many', 'pgrowers' ),
        ),
        'default' => 'standard',
    ) );



}
