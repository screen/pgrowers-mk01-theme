<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_center', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_center() {
    if (is_singular()) {
        echo '<section id="main" class="container"><div class="row"><div class="woocustom-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">';
    } else {
        echo '<section id="main" class="container-fluid"><div class="row"><div class="woocustom-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">';
    }
}

function my_theme_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

add_action('woocommerce_before_main_content', 'woocommerce_custom_back_btn', 20);


function woocommerce_custom_back_btn() {
    if (is_singular()) { ?>
<button class="btn btn-md btn-back">
    <?php _e('Back'); ?></button>
<?php   }
}

add_action('woocommerce_single_product_summary' ,'woocommerce_breadcrumb', 1);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_after_single_product_summary', 'woocommerce_custom_product_values', 15);
add_action('woocommerce_after_single_product_summary', 'woocommerce_custom_color_values', 14);


remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);


/* REMOVE PRICE ELEMENTS */
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);

function woocommerce_custom_color_values() {
    global $post;

    $terms = get_the_terms(get_the_ID(), array('taxonomy' => 'pa_color'));
    $found_post = null;

    if ( $posts = get_posts( array(
        'name' => $terms[0]->slug,
        'post_type' => 'color-guide',
        'posts_per_page' => 1
    ) ) ) $found_post = $posts[0];

    // Now, we can do something with $found_post
    if ( ! is_null( $found_post ) ){
?>
<div class="w-100"></div>
<div class="rose-color-guide-single col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="row align-items-center">
        <div class="rose-color-guide-single-picture col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
            <div class="rose-color-guide-single-image">
                <?php echo get_the_post_thumbnail($found_post, 'full', array('class' => 'img-fluid')); ?>
            </div>
        </div>
        <div class="rose-color-guide-single-info col-xl-9 col-lg-9 col-md-8 col-sm-8 col-12">
            <h2>
                <?php echo $found_post->post_title; ?>
            </h2>
            <?php $content = apply_filters('the_content', $found_post->post_content); ?>
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php
                                   }
}


function woocommerce_custom_product_values() {
    global $post;
?>
<div class="w-100"></div>
<div class="fact-sheet-container">
    <h2>
        <?php _e('Fact Sheet', 'pgrowers'); ?>
    </h2>
    <div class="container p-0">
        <div class="row">
            <div class="fact-sheet-item fact-sheet-item-odd col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_vase_life', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Vase Life', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-even col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_fragance', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Fragance', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-odd col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_bloom_size', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Bloom Size', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-even col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_petal_count', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Petal Count', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-odd col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_aperture', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Aperture', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-even col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_floiage', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Foliage', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-odd fact-sheet-item-last col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_aperture_speed', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Aperture Speed', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fact-sheet-item fact-sheet-item-even fact-sheet-item-last col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $post_meta = get_post_meta($post->ID, 'pg_product_thorn_count', true); ?>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h3>
                                <?php _e('Thorn Count', 'pgrowers'); ?>
                            </h3>
                        </div>
                        <div class="fact-sheet-item-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo get_fact_sheet_item($post_meta); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}

function get_fact_sheet_item($value_post_meta) {
    if ($value_post_meta == 'slow' || $value_post_meta == 'short' || $value_post_meta == 'slight' || $value_post_meta == 'light' || $value_post_meta == 'few' || $value_post_meta == 'small' ) { $value = 33.33; }
    if ($value_post_meta == 'medium') { $value = 66.66; }
    if ($value_post_meta == 'fast' || $value_post_meta == 'long' || $value_post_meta == 'large' || $value_post_meta == 'many' || $value_post_meta == 'heavy' || $value_post_meta == 'strong') { $value = 100; }
?>
<div class="progress">
    <div class="progress-bar" role="progressbar" style="width: <?php echo $value; ?>%" aria-valuenow="<?php echo $value; ?>" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<label class="fact-sheet-item-label">
    <?php echo $value_post_meta; ?></label>
<?php
}



add_action('pa_color_edit_form_fields','pa_color_edit_form_fields');
add_action('pa_color_add_form_fields','pa_color_edit_form_fields');
add_action('edited_pa_color', 'pa_color_save_form_fields', 10, 2);
add_action('created_pa_color', 'pa_color_save_form_fields', 10, 2);

function pa_color_save_form_fields($term_id) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker');
    wp_enqueue_script('admin-woocommerce-functions', get_template_directory_uri() . '/js/admin-woocommerce-functions.js', array( 'wp-color-picker' ), false, true );

    if( isset( $_POST['pa_color_custom_color'] ) && ! empty( $_POST['pa_color_custom_color'] ) ) {
        update_term_meta( $term_id, 'pa_color_custom_color', sanitize_hex_color_no_hash( $_POST['pa_color_custom_color'] ) );
    } else {
        delete_term_meta( $term_id, 'pa_color_custom_color' );
    }
}

function pa_color_edit_form_fields ($term_obj) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker');
    wp_enqueue_script('admin-woocommerce-functions', get_template_directory_uri() . '/js/admin-woocommerce-functions.js', array( 'wp-color-picker' ), false, true );
    // Read in the order from the options db
    $color = get_term_meta( $term_obj->term_id, 'pa_color_custom_color', true );

    $color = ( ! empty( $color ) ) ? "#{$color}" : '#000';

?>
<tr class="form-field">
    <th valign="top" scope="row">
        <label for="pa_color_custom_color">
            <?php _e('Color:', ''); ?></label>
    </th>
    <td>
        <input type="text" id="pa_color_custom_color" name="pa_color_custom_color" class="woocommerce-custom-color-field" value="<?php echo $color; ?>" />
    </td>
</tr>
<?php
}
