<?php
/* --------------------------------------------------------------
/* ABOUT MAIN - METABOX - SECTION 1
-------------------------------------------------------------- */
$cmb_about_main_page_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'about_main_metabox',
    'title'         => __( 'Sección: Iconos Principales', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about', 'alt_value' => 'page-about.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_about_main_page_metabox->add_field( [
    'id'      => $prefix . 'about_main_icons',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Ícono {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Ícono', 'pgrowers' ),
        'remove_button' => __( 'Remover Ícono', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_main_page_metabox->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Ícono', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_main_icon_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_about_main_page_metabox->add_group_field( $group_field_id, [
    'name'         => __('Texto del Ícono:', 'pgrowers'),
    'desc'         => __( 'Ingrese el texto del Ícono', 'pgrowers'),
    'id'      => $prefix . 'about_main_icon_text',
    'type'    => 'text'
] );

/* --------------------------------------------------------------
/* ABOUT MAIN - METABOX - SECTION 2
-------------------------------------------------------------- */
$cmb_about_main_page_metabox2 = new_cmb2_box( array(
    'id'            => $prefix . 'about_main_double_desc_metabox',
    'title'         => __( 'Sección: Descripción', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about', 'alt_value' => 'page-about.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_about_main_page_metabox2->add_field( [
    'id'      => $prefix . 'about_page_double_desc_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Descripción {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Descripción', 'pgrowers' ),
        'remove_button' => __( 'Remover Descripción', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_main_page_metabox2->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'about_main_double_desc',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

/* --------------------------------------------------------------
/* ABOUT MAIN - METABOX - SECTION 3
-------------------------------------------------------------- */

$cmb_about_main_page_metabox3 = new_cmb2_box( array(
    'id'            => $prefix . 'about_main_values_metabox',
    'title'         => __( 'Sección: Core Values', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about', 'alt_value' => 'page-about.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_main_page_metabox3->add_field( array(
    'name' => __( 'Título de Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el título principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_main_values_title',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );

$group_field_id = $cmb_about_main_page_metabox3->add_field( [
    'id'      => $prefix . 'about_main_values_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Item {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Item', 'pgrowers' ),
        'remove_button' => __( 'Remover Item', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_main_page_metabox3->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Ícono', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_main_values_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_about_main_page_metabox3->add_group_field( $group_field_id, [
    'name'         => __('Texto del Ícono:', 'pgrowers'),
    'desc'         => __( 'Ingrese el texto del Ícono', 'pgrowers'),
    'id'      => $prefix . 'about_main_values_text',
    'type'    => 'text'
] );


/* --------------------------------------------------------------
/* ABOUT OUR PEOPLE - METABOX - SECTION 1
-------------------------------------------------------------- */
$cmb_about_people_page_metabox1 = new_cmb2_box( array(
    'id'            => $prefix . 'about_people_section1_metabox',
    'title'         => __( 'Sección: Sección 1', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about-people', 'alt_value' => 'page-about-people.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_about_people_page_metabox1->add_field( [
    'id'      => $prefix . 'about_people_img_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Imagen {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Imagen', 'pgrowers' ),
        'remove_button' => __( 'Remover Imagen', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_people_page_metabox1->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_people_scalonated_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

/* --------------------------------------------------------------
/* ABOUT OUR PEOPLE - METABOX - SECTION 2
-------------------------------------------------------------- */

$cmb_about_people_page_metabox2 = new_cmb2_box( array(
    'id'            => $prefix . 'about_people_section2_metabox',
    'title'         => __( 'Sección: Slider de 3 Imágenes', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about-people', 'alt_value' => 'page-about-people.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_about_people_page_metabox2->add_field( [
    'id'      => $prefix . 'about_people_slider_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Slide {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Slide', 'pgrowers' ),
        'remove_button' => __( 'Remover Slide', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_people_page_metabox2->add_group_field( $group_field_id, [
    'name'         => __( 'Slide', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_people_slider_img',
    'type'         => 'file',
    'preview_size' => 'thumbnail',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Slide', 'pgrowers' )
    ),
] );



/* --------------------------------------------------------------
/* ABOUT PASSION - METABOX - SECTION 1
-------------------------------------------------------------- */

$cmb_about_passion_page_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'about_passion_metabox',
    'title'         => __( 'Sección: Descripción', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about-passion', 'alt_value' => 'page-about-passion.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_about_passion_page_metabox->add_field( [
    'id'      => $prefix . 'about_people_img_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Imagen {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Imagen', 'pgrowers' ),
        'remove_button' => __( 'Remover Imagen', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_passion_page_metabox->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_people_scalonated_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_about_passion_page_metabox->add_field( array(
    'name' => __( 'Contenido de la Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el Contenido principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_main_values_content',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );

$cmb_about_passion_page_metabox2 = new_cmb2_box( array(
    'id'            => $prefix . 'about_passion_metabox2',
    'title'         => __( 'Sección: Core Values', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about-passion', 'alt_value' => 'page-about-passion.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_passion_page_metabox2->add_field( array(
    'name' => __( 'Contenido de la Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el Contenido principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_core_values_content',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );

$cmb_about_passion_page_metabox2->add_field( array(
    'name' => __( 'Imagen de Fondo de la Sección', 'cmb2' ),
    'desc' => __( 'Ingrese la Imagen principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_core_items_background',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
) );

$group_field_id = $cmb_about_passion_page_metabox2->add_field( [
    'id'      => $prefix . 'about_core_values_item_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Item {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Item', 'pgrowers' ),
        'remove_button' => __( 'Remover Item', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_passion_page_metabox2->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Ícono', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_core_values_item_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_about_passion_page_metabox2->add_group_field( $group_field_id, [
    'name'         => __('Texto del Ícono:', 'pgrowers'),
    'desc'         => __( 'Ingrese el texto del Ícono', 'pgrowers'),
    'id'      => $prefix . 'about_core_values_item_text',
    'type'    => 'textarea'
] );

$cmb_about_passion_page_metabox3 = new_cmb2_box( array(
    'id'            => $prefix . 'about_passion_metabox3',
    'title'         => __( 'Sección: Enviroment Values', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'about-passion', 'alt_value' => 'page-about-passion.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_passion_page_metabox3->add_field( array(
    'name' => __( 'Contenido de la Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el Contenido principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_enviroment_content',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );

$group_field_id = $cmb_about_passion_page_metabox3->add_field( [
    'id'      => $prefix . 'about_enviroment_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Imagen {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Imagen', 'pgrowers' ),
        'remove_button' => __( 'Remover Imagen', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_about_passion_page_metabox3->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'about_enviroment_scalonated_img',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_about_passion_page_metabox3->add_field( array(
    'name' => __( 'Contenido de la Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el Contenido principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'about_enviroment_description',
    'type' => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
) );
