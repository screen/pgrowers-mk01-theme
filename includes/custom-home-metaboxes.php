<?php

/* GET SHOP CATEGORY */
function get_product_cat() {
    $shop_category = array();
    $return_categories = array();
    $shop_category = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false));
    if ( ! empty( $shop_category ) && ! is_wp_error( $shop_category ) ){
        foreach ( $shop_category as $shop_cat_item ) {
            $return_categories[$shop_cat_item->term_id] = $shop_cat_item->name;
        }
    }

    return $return_categories;
}

/* GET SHOP CATEGORY */

/* --------------------------------------------------------------
/* HOME METABOX FUNCTIONS
-------------------------------------------------------------- */
/* --------------------------------------------------------------
/* SLIDER METABOX
-------------------------------------------------------------- */
$cmb_home_slider = new_cmb2_box( array(
    'id'            => $prefix . 'home_slider_metabox',
    'title'         => __( 'Sección: Slider', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_home_slider->add_field( [
    'id'      => $prefix . 'home_slide_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Slide {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Slide', 'pgrowers' ),
        'remove_button' => __( 'Remover Slide', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_home_slider->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Slide', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'home_slide_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_home_slider->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'home_slide_content',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

$cmb_home_slider->add_group_field( $group_field_id, [
    'name'         => __('Texto del Botón:', 'pgrowers'),
    'desc'         => __( 'Ingrese el texto del boton de este slide', 'pgrowers'),
    'id'      => $prefix . 'home_slide_btn_text',
    'type'    => 'text'
] );

$cmb_home_slider->add_group_field( $group_field_id, [
    'name'         => __('URL del Botón:', 'pgrowers'),
    'desc'         => __( 'Ingrese la dirección URL del boton de este slide', 'pgrowers' ),
    'id'      => $prefix . 'home_slide_btn_url',
    'type' => 'text_url',
] );

/* --------------------------------------------------------------
/* PROFILE ABOUT METABOX
-------------------------------------------------------------- */
$cmb_home_profile = new_cmb2_box( array(
    'id'            => $prefix . 'home_profile_metabox',
    'title'         => __( 'Sección: Perfiles', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_home_profile->add_field( [
    'id'      => $prefix . 'home_profile_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Perfil {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Perfil', 'pgrowers' ),
        'remove_button' => __( 'Remover Perfil', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Perfil', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'home_profile_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'home_profile_content',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('Texto del Botón:', 'pgrowers'),
    'desc'         => __( 'Ingrese el texto del boton de este slide', 'pgrowers'),
    'id'      => $prefix . 'home_profile_btn_text',
    'type'    => 'text'
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('URL del Botón:', 'pgrowers'),
    'desc'         => __( 'Ingrese la dirección URL del boton de este slide', 'pgrowers' ),
    'id'      => $prefix . 'home_profile_btn_url',
    'type' => 'text_url',
] );

/* --------------------------------------------------------------
/* SHOP CATEGORY METABOX
-------------------------------------------------------------- */
$cmb_home_shop_category = new_cmb2_box( array(
    'id'            => $prefix . 'home_shop_category_metabox',
    'title'         => __( 'Sección: Shop Category', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_home_shop_category->add_field( [
    'id'      => $prefix . 'home_shop_category_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Categoría {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Categoría', 'pgrowers' ),
        'remove_button' => __( 'Remover Categoría', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_home_shop_category->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen de Categoria', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'home_shop_category_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_home_shop_category->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'home_shop_category_selected',
    'type'    => 'select',
    'options' => get_product_cat()
] );

/* --------------------------------------------------------------
/* ROSE SPECIAL METABOX
-------------------------------------------------------------- */
$cmb_home_rose = new_cmb2_box( array(
    'id'            => $prefix . 'home_rose_special_metabox',
    'title'         => __( 'Sección: Rose & Flowers Special', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_home_rose->add_field( array(
    'name' => __( 'Título de Sección', 'cmb2' ),
    'desc' => __( 'Ingrese el título principal de esta sección', 'cmb2' ),
    'id'   => $prefix . 'home_rose_special_title',
    'type' => 'text'
) );

$cmb_home_rose->add_field( array(
    'name' => __( 'Ícono de Sección', 'cmb2' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'   => $prefix . 'home_rose_special_icon',
    'type' => 'file',
    'preview_size' => [ 300, 300 ]
) );

$group_field_id = $cmb_home_rose->add_field( [
    'id'      => $prefix . 'home_rose_special_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Item {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Item', 'pgrowers' ),
        'remove_button' => __( 'Remover Item', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_home_rose->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Perfil', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'home_rose_special_item_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_home_rose->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'home_rose_special_item_content',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );
/* --------------------------------------------------------------
/* FEATURED IN SECTION
-------------------------------------------------------------- */
$cmb_featured_slider = new_cmb2_box( array(
    'id'            => $prefix . 'home_featured_metabox',
    'title'         => __( 'Sección: Featured In', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_featured_slider->add_field( [
    'id'      => $prefix . 'home_featured_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Ícono {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Ícono', 'pgrowers' ),
        'remove_button' => __( 'Remover Ícono', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_featured_slider->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Ícono', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'home_featured_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_featured_slider->add_group_field( $group_field_id, [
    'name'         => __('URL del Ícono:', 'pgrowers'),
    'desc'         => __( 'Ingrese la dirección URL del boton de este slide', 'pgrowers' ),
    'id'      => $prefix . 'home_featured_url',
    'type' => 'text_url',
] );
