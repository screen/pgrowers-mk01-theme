<?php
/* --------------------------------------------------------------
/* ROSE - FLOWERS METABOX FUNCTIONS
-------------------------------------------------------------- */
/* --------------------------------------------------------------
/* FILTER SECTION
-------------------------------------------------------------- */
$cmb_rose_filters = new_cmb2_box( array(
    'id'            => $prefix . 'rose_flowers_metabox',
    'title'         => __( 'Sección: Slider', 'cmb2' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'slug', 'value' => 'our-roses-flowers', 'alt_value' => 'page-our-roses-flowers.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );



$group_field_id = $cmb_rose_filters->add_field( [
    'id'      => $prefix . 'rose_flowers_filter_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Filter {#}', 'pgrowers' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Filter', 'pgrowers' ),
        'remove_button' => __( 'Remover Filter', 'pgrowers' ),
        'sortable'      => true, // beta
        'closed'        => true, // true to have the groups closed by default
    ),
] );

$cmb_rose_filters->add_group_field( $group_field_id, [
    'name'         => __( 'Imagen del Slide', 'pgrowers' ),
    'desc'         => __( 'NOTA: Debe ser una imagen en JPG / PNG / BMP', 'pgrowers' ),
    'id'           => $prefix . 'rose_flowers_filter_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __( 'Cargar Imagen', 'pgrowers' )
    ),
] );

$cmb_rose_filters->add_group_field( $group_field_id, [
    'name'         => __( 'Contenido:', 'pgrowers' ),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'pgrowers'),
    'id'      => $prefix . 'rose_flowers_filter_content',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

$cmb_rose_filters->add_group_field( $group_field_id, [
    'name'         => __('URL del Botón:', 'pgrowers'),
    'desc'         => __( 'Ingrese la dirección URL del boton de este slide', 'pgrowers' ),
    'id'      => $prefix . 'rose_flowers_filter_url',
    'type' => 'text_url',
] );
