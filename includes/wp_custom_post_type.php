<?php

// Register Custom Post Type
function pgrowers_rose_type() {

    $labels = array(
        'name'                  => _x( 'Rose Colors', 'Post Type General Name', 'pgrowers' ),
        'singular_name'         => _x( 'Rose Color', 'Post Type Singular Name', 'pgrowers' ),
        'menu_name'             => __( 'Rose Color', 'pgrowers' ),
        'name_admin_bar'        => __( 'Rose Color', 'pgrowers' ),
        'archives'              => __( 'Rose Color Archives', 'pgrowers' ),
        'attributes'            => __( 'Rose Color Attributes', 'pgrowers' ),
        'parent_item_colon'     => __( 'Parent Rose Color:', 'pgrowers' ),
        'all_items'             => __( 'All Rose Colors', 'pgrowers' ),
        'add_new_item'          => __( 'Add New Rose Color', 'pgrowers' ),
        'add_new'               => __( 'Add New', 'pgrowers' ),
        'new_item'              => __( 'New Rose Color', 'pgrowers' ),
        'edit_item'             => __( 'Edit Rose Color', 'pgrowers' ),
        'update_item'           => __( 'Update Rose Color', 'pgrowers' ),
        'view_item'             => __( 'View Rose Color', 'pgrowers' ),
        'view_items'            => __( 'View Rose Color', 'pgrowers' ),
        'search_items'          => __( 'Search Rose Color', 'pgrowers' ),
        'not_found'             => __( 'Not found', 'pgrowers' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'pgrowers' ),
        'featured_image'        => __( 'Product Image', 'pgrowers' ),
        'set_featured_image'    => __( 'Set Product image', 'pgrowers' ),
        'remove_featured_image' => __( 'Remove Product image', 'pgrowers' ),
        'use_featured_image'    => __( 'Use as Product image', 'pgrowers' ),
        'insert_into_item'      => __( 'Insert into Rose Color', 'pgrowers' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Rose Color', 'pgrowers' ),
        'items_list'            => __( 'Rose Colors list', 'pgrowers' ),
        'items_list_navigation' => __( 'Rose Colors list navigation', 'pgrowers' ),
        'filter_items_list'     => __( 'Filter Rose Colors list', 'pgrowers' ),
    );
    $args = array(
        'label'                 => __( 'Rose Color', 'pgrowers' ),
        'description'           => __( 'Rose Color Guide', 'pgrowers' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-palmtree',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'color-guide', $args );

}
add_action( 'init', 'pgrowers_rose_type', 0 );

// Register Custom Post Type
function custom_glossary() {

    $labels = array(
        'name'                  => _x( 'Words', 'Post Type General Name', 'pgrowers' ),
        'singular_name'         => _x( 'Word', 'Post Type Singular Name', 'pgrowers' ),
        'menu_name'             => __( 'Glossary', 'pgrowers' ),
        'name_admin_bar'        => __( 'Glossary', 'pgrowers' ),
        'archives'              => __( 'Glossary Archives', 'pgrowers' ),
        'attributes'            => __( 'Word Attributes', 'pgrowers' ),
        'parent_item_colon'     => __( 'Parent Word:', 'pgrowers' ),
        'all_items'             => __( 'All Words', 'pgrowers' ),
        'add_new_item'          => __( 'Add New Word', 'pgrowers' ),
        'add_new'               => __( 'Add New', 'pgrowers' ),
        'new_item'              => __( 'New Word', 'pgrowers' ),
        'edit_item'             => __( 'Edit Word', 'pgrowers' ),
        'update_item'           => __( 'Update Word', 'pgrowers' ),
        'view_item'             => __( 'View Word', 'pgrowers' ),
        'view_items'            => __( 'View Words', 'pgrowers' ),
        'search_items'          => __( 'Search Word', 'pgrowers' ),
        'not_found'             => __( 'Not found', 'pgrowers' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'pgrowers' ),
        'featured_image'        => __( 'Featured Image', 'pgrowers' ),
        'set_featured_image'    => __( 'Set featured image', 'pgrowers' ),
        'remove_featured_image' => __( 'Remove featured image', 'pgrowers' ),
        'use_featured_image'    => __( 'Use as featured image', 'pgrowers' ),
        'insert_into_item'      => __( 'Insert into Word', 'pgrowers' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Word', 'pgrowers' ),
        'items_list'            => __( 'Glossary list', 'pgrowers' ),
        'items_list_navigation' => __( 'Glossary list navigation', 'pgrowers' ),
        'filter_items_list'     => __( 'Filter Glossary list', 'pgrowers' ),
    );
    $args = array(
        'label'                 => __( 'Word', 'pgrowers' ),
        'description'           => __( 'Glossary of Terms', 'pgrowers' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-book-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'glossary', $args );

}
add_action( 'init', 'custom_glossary', 0 );
