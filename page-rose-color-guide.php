<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="color-guide-page-section col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="w-100"></div>
                </div>
            </div>

        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

            <div class="container">
                <div class="row align-items-start justify-content-center">
                    <div class="color-guide-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <?php $args = array('post_type' => 'color-guide', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date'); ?>
                                <?php $color_array = new WP_Query($args); ?>
                                <?php if ($color_array->have_posts()) : ?>
                                <div class="table-responsive">
                                    <table class="table table-hover table-custom">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Color</th>
                                                <th>Meaning</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($color_array->have_posts()) : $color_array->the_post(); ?>
                                            <?php get_template_part('templates/rose-color-guide-item'); ?>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $quote = get_post_meta(get_the_ID(), 'pg_color_guide_quote', true); ?>
                <?php echo apply_filters('the_content', $quote); ?>
            </div>
        </section>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
