<?php get_header(); ?>
<?php the_post(); ?>
<main class="container">
    <div class="row">
        <div class="page-section single-breadcrumb-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul>
                <li><a href="<?php echo home_url('/blog'); ?>" title="<?php _e('Back to Blog', 'pgrowers'); ?>"><?php _e('Blog')?></a> / </li>
                <li><?php echo get_the_title(); ?></li>
            </ul>
        </div>
        <div class="single-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>"  class="single-container col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12  <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                        <?php if ( has_post_thumbnail()) : ?>
                        <picture>
                            <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                        </picture>
                        <?php endif; ?>
                        <header>
                            <div class="date"><?php the_time('F j, Y'); ?></div>
                            <h1 itemprop="name"><?php the_title(); ?></h1>
                        </header>
                        <?php the_content() ?>
                        <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'pgrowers' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                        <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                        <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                        <meta itemprop="url" content="<?php the_permalink() ?>">

                    </article>
                    <aside class="blog-sidebar col-xl-3 offset-xl-1 col-lg-3 offset-lg-1 d-xl-block d-lg-block d-md-none d-sm-none d-none" role="complementary">
                        <?php get_sidebar(); ?>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
