<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>

        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php $about_double_desc = get_custom_metabox_group(get_the_ID(), 'pg_about_page_double_desc_group'); ?>
            <?php if (!empty($about_double_desc)) { ?>
            <div class="container">
                <div class="row">
                    <div class="about-page-title-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="page-text-section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row align-items-top justify-content-center">
                                <?php foreach ($about_double_desc as $desc_item) { ?>
                                <div class="page-text-section col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                                    <?php echo apply_filters( 'the_content', $desc_item['pg_about_main_double_desc'] ); ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php $about_values_array = get_custom_metabox_group(get_the_ID(), 'pg_about_main_values_group'); ?>
            <?php if (!empty($about_values_array)) { ?>
            <div class="container">
                <div class="row">
                    <div class="about-page-title-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="text-center">
                            <?php echo get_post_meta( get_the_ID(), 'pg_about_main_values_title', true ); ?>
                        </h2>
                    </div>
                    <?php foreach ( $about_values_array as $values_item ) {?>
                    <div class="about-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="page-item-img col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo wp_get_attachment_image( $values_item['pg_about_main_values_img_id'], 'vertical_img', '', array( "class" => "img-fluid" )  ); ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
