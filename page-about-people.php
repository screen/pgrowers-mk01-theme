<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="about-img-section col-xl-6 col-lg-6 col-md-6 col-sm-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $about_people_images = get_custom_metabox_group(get_the_ID(), 'pg_about_people_img_group'); ?>
                        <?php if (!empty($about_people_images)) { ?>
                        <div class="page-img-overlayed">
                            <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                            <div class="page-img-overlayed-item page-img-overlayed-item-<?php echo $i; ?>">
                                <?php echo wp_get_attachment_image( $feature['pg_about_people_scalonated_img_id'], 'medium', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="about-text-section col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php $about_slider_images = get_custom_metabox_group(get_the_ID(), 'pg_about_people_slider_group'); ?>
                    <?php if (!empty($about_slider_images)) { ?>
                    <div class="page-section-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-section-slider owl-carousel owl-theme">
                            <?php foreach ( $about_slider_images as $feature ) {?>
                            <div class="item">
                                <?php echo wp_get_attachment_image( $feature['pg_about_people_slider_img_id'], 'full', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
