<div class="container-fluid">
    <div class="row">
        <div class="home-rose-special-container col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="home-rose-special-title col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <h2>
                            <?php echo get_post_meta(get_the_ID(), 'pg_home_rose_special_title', true); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-rose-special-content col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <?php $home_rose_array = get_custom_metabox_group(get_the_ID(), 'pg_home_rose_special_group'); ?>
            <?php if (!empty($home_rose_array)) { ?>
            <div class="container">
                <div class="row align-items-top justify-content-center">
                    <?php $count_delay = 50; ?>
                    <div class="home-rose-special-item d-lg-none d-xl-none col-md-4 col-sm-6 col-12 d-md-block d-sm-block d-block" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item1[] = $home_rose_array[1]; ?>
                        <?php $rose_item1 = array_shift($rose_item1); ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item1['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                            <?php echo apply_filters( 'the_content', $rose_item1['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <div class="home-rose-special-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item[] = $home_rose_array[0]; ?>
                        <?php $rose_item = array_shift($rose_item); ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                            <?php echo apply_filters( 'the_content', $rose_item['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <div class="home-rose-special-item d-lg-none d-xl-none col-md-4 col-sm-6 col-12 d-md-block d-sm-block d-block" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item2[] = $home_rose_array[2]; ?>
                        <?php $rose_item2 = array_shift($rose_item2); ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item2['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                            <?php echo apply_filters( 'the_content', $rose_item2['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <div class="w-100 d-lg-block d-xl-block d-md-block d-sm-none d-none"></div>
                    <?php $count_delay = 50; ?>
                    <div class="home-rose-special-item d-lg-block d-xl-block col-xl-3 col-lg-3 d-md-none d-sm-none d-none" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item11[] = $home_rose_array[1]; ?>
                        <?php $rose_item11 = array_shift($rose_item11); ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item11['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                            <?php echo apply_filters( 'the_content', $rose_item11['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <div class="home-rose-special-rose col-xl-6 col-lg-6 col-md-12 d-lg-block d-xl-block d-md-block d-sm-none d-none" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="0">
                        <img src="<?php echo esc_url(get_post_meta(get_the_ID(), 'pg_home_rose_special_icon', true)); ?>" alt="" class="img-fluid">
                    </div>
                    <div class="home-rose-special-item d-lg-block d-xl-block col-xl-3 col-lg-3 d-md-none d-sm-none d-none" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item22[] = $home_rose_array[2]; ?>
                        <?php $rose_item22 = array_shift($rose_item22); ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item22['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                            <?php echo apply_filters( 'the_content', $rose_item22['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <div class="w-100 d-lg-block d-xl-block d-md-block d-sm-none d-none"></div>
                    <?php $y = 1; ?>
                    <?php $count_delay = 50; ?>
                    <?php for ($i = 3; $i <= 5; $i++) { ?>
                    <div class="home-rose-special-item home-rose-special-item-last-<?php echo $y; ?> col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                        <?php $rose_item = array(); ?>
                        <?php $rose_item = $home_rose_array[$i]; ?>
                        <div class="home-about-item-img">
                            <img src="<?php echo esc_url($rose_item['pg_home_rose_special_item_image']); ?>" alt="" class="img-fluid" />
                        </div>
                        <div class="home-about-item-content">
                             <?php echo apply_filters( 'the_content', $rose_item['pg_home_rose_special_item_content'] ); ?>
                        </div>
                    </div>
                    <?php $count_delay = $count_delay + 50; ?>
                    <?php $y++; }?>

                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
