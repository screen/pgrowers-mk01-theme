<div class="container">
    <div class="row">
        <div class="page-section-slider-container col-12">
            <div class="page-section-slider owl-carousel owl-theme">
                <?php $features_group = (array) get_post_meta( get_the_ID(), 'features_group', true ); ?>
                <?php foreach ( $features_group as $feature ) {?>
                <div class="item">
                    <?php echo wp_get_attachment_image( $feature['image_id'], 'full', false, array('class' => 'img-fluid') ); ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php wp_reset_query(); ?>
