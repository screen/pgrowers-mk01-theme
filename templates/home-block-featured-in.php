<?php $home_featured_array = get_custom_metabox_group(get_the_ID(), 'pg_home_featured_group'); ?>
<?php if (!empty($home_featured_array)) { ?>
<section class="home-featured-in col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row align-items-start justify-content-center">
            <?php $count_delay = 50; ?>
            <?php foreach ( $home_featured_array as $featured_item ) {?>
            <div class="home-about-item col-xl col-lg col-md col-sm-6 col-6" data-aos="fade-in" data-aos-duration="10000" data-aos-delay="<?php echo $count_delay; ?>">
                <div class="home-about-item-img">
                    <a href="<?php echo $featured_item['pg_home_featured_url']; ?>" title="<?php _e('Visitar sitio'); ?>">
                        <img src="<?php echo $featured_item['pg_home_featured_image']; ?>" alt="<?php _e('Featured in'); ?>" class="img-fluid"/>
                    </a>
                </div>
            </div>
            <?php $count_delay = $count_delay + 50; ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
