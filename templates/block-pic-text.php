<div class="container">
    <div class="row">
        <div class="page-img-section col-6">
            <div class="page-img-overlayed">
                <?php $features_group = (array) get_post_meta( get_the_ID(), 'features_group', true ); ?>
                <?php $i=1; foreach ( $features_group as $feature ) {?>
                <div class="page-img-overlayed-item page-img-overlayed-item-<?php echo $i; ?>">
                    <?php echo wp_get_attachment_image( $feature['image_id'], 'medium', false, array('class' => 'img-fluid') ); ?>
                </div>
                <?php $i++; } ?>
            </div>
        </div>
        <div class="page-text-section col-5">
            <?php the_content(); ?>
        </div>
    </div>
</div>
