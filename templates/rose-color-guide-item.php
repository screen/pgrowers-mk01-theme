<tr>
    <td class="p-0">
        <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
    </td>
    <td >
        <div class="color-rose-icon" style="background-color: <?php echo get_post_meta(get_the_ID(), 'pg_guide_color_icon', true);?>">

        </div>
        <p class="text-center">
            <?php the_title(); ?>
        </p>
    </td>
    <td>
        <?php the_content(); ?>
    </td>
    <td>
        <a href="<?php echo home_url('/shop'); ?>?filter_color=<?php echo get_the_title(); ?>" title="<?php _e('View Rose', 'pgrowers'); ?>" class="btn btn-md btn-view"><?php _e('View Rose', 'pgrowers'); ?></a>
    </td>
</tr>
