<?php $home_slider_array = get_custom_metabox_group(get_the_ID(), 'pg_home_slide_group'); ?>
<?php if (!empty($home_slider_array)) { ?>
<section class="the-slider col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
    <div class="container-fluid">
        <div class="row">
            <div class="slider-container owl-carousel owl-theme col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <?php foreach($home_slider_array as $slider_item) { ?>
                <div class="slide-item col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 item" style="background: url(<?php echo $slider_item['pg_home_slide_image']; ?>);">
                    <div class="container">
                        <div class="row slide-row align-items-center justify-content-center">
                            <div class="slide-content col-12 col-xl-10 col-lg-10 col-md-10 col-sm-12">
                               <?php echo apply_filters('the_content', $slider_item['pg_home_slide_content']); ?>
                                <a href="<?php echo esc_url($slider_item['pg_home_slide_btn_url']); ?>" title="<?php echo esc_html($slider_item['pg_home_slide_btn_text']); ?>" class="btn btn-md btn-slide">
                                    <?php echo esc_html($slider_item['pg_home_slide_btn_text']); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>
