<div class="page-item page-item-scalated page-item-scalated-<?php echo $i; ?> col-4">
    <div class="page-item-img">
        <?php echo wp_get_attachment_image( $feature['image_id'], 'full', false, array('class' => 'img-fluid') ); ?>
    </div>
    <div class="page-item-content">
        <?php echo apply_filters( 'the_content', $feature['description'] ); ?>
    </div>
</div>
