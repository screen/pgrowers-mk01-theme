<?php $home_profile_array = get_custom_metabox_group(get_the_ID(), 'pg_home_profile_group'); ?>
<?php if (!empty($home_profile_array)) { ?>
<section class="home-about col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
    <div class="container">
        <div class="row">
            <?php $count_delay = 50; ?>
            <?php foreach ( $home_profile_array as $profile_item ) {?>
            <div class="home-about-item col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" data-aos="slide-up" data-aos-duration="10000"  data-aos-delay="<?php echo $count_delay; ?>">
                <div class="home-about-item-img">
                   <img src="<?php echo esc_url($profile_item['pg_home_profile_image']); ?>" alt="Image" class="img-fluid" />
                </div>
                <div class="home-about-item-content">
                    <?php echo apply_filters( 'the_content', $profile_item['pg_home_profile_content'] ); ?>
                    <a href="<?php echo esc_url($profile_item['pg_home_profile_btn_url']); ?>" title="<?php echo esc_html($profile_item['pg_home_profile_btn_text']); ?>" class="btn-md btn-profile"><?php echo esc_html($profile_item['pg_home_profile_btn_text']); ?></a>
                </div>
            </div>
            <?php $count_delay = $count_delay + 50; ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
