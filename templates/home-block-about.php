<section class="home-about col-12">
    <div class="container">
        <div class="row">
            <?php $count_delay = 50; ?>
            <?php $features_group = (array) get_post_meta( get_the_ID(), 'features_group', true ); ?>
            <?php foreach ( $features_group as $feature ) {?>
            <div class="home-about-item col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" data-aos="slide-up" data-aos-duration="10000"  data-aos-delay="<?php echo $count_delay; ?>">
                <div class="home-about-item-img">
                    <?php echo wp_get_attachment_image( $feature['image_id'], 'full', false, array('class' => 'img-fluid') ); ?>
                </div>
                <div class="home-about-item-content">
                    <?php echo apply_filters( 'the_content', $feature['description'] ); ?>
                </div>
            </div>
            <?php $count_delay = $count_delay + 50; ?>
            <?php } ?>
        </div>
    </div>
</section>
