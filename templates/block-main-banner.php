<?php $banner_id = get_post_meta(get_the_ID(), 'pg_page_general_banner_id', true); ?>
<?php echo wp_get_attachment_image( $banner_id, 'banner_img' ); ?>
<div class="page-banner-title">
    <h1>
        <?php the_title(); ?>
    </h1>
</div>
