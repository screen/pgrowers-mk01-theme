<?php $home_category_array = get_custom_metabox_group(get_the_ID(), 'pg_home_shop_category_group'); ?>
<?php if (!empty($home_category_array)) { ?>
<div class="container-fluid">
    <div class="row">
        <?php foreach ($home_category_array as $home_category_item) { ?>
        <div class="shop-category-item col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12" data-aos="fade-in" data-aos-duration="4000">
            <?php $cat_object = get_term_by('id', $home_category_item['pg_home_shop_category_selected'], 'product_cat'); ?>
            <div class="shop-category-item-wrapper" style="background: url(<?php echo esc_url($home_category_item['pg_home_shop_category_image']); ?>); ">
                <a href="<?php echo get_term_link($cat_object, 'product_cat'); ?>" title="<?php echo $cat_object->name; ?>" class="btn btn-md btn-shop-category">
                    <?php echo $cat_object->name; ?></a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
