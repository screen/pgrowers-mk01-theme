<form action="" class="form custom-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="row">
        <div class="custom-form-control-container col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <label for="fullname"><input type="text" name="fullname" class="form-control custom-form-control" /><span>Full Name</span></label>
        </div>
        <div class="custom-form-control-container col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <label for="phone"><input type="tel" name="phone" class="form-control custom-form-control" /><span>Phone</span></label>
        </div>
        <div class="custom-form-control-container col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <label for="email"><input type="email" name="email" class="form-control custom-form-control" /><span>E-Mail Address</span></label>
        </div>
        <div class="custom-form-control-radio-container col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <label for="got_roses"><span>I got my Roses</span></label>
            <input type="radio" name="got_roses[]" class="form-control custom-form-control custom-radio-control" /><span class="label-control">As a Gift</span>
            <input type="radio" name="got_roses[]" class="form-control custom-form-control custom-radio-control" /><span class="label-control">I purchased them</span>
        </div>
        <div class="custom-form-control-container col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <label for="store"><input type="text" name="store" class="form-control custom-form-control" /><span>Store/Shipper Name</span></label>
        </div>
        <div class="custom-form-control-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <label for="comments">
                <textarea name="comments" id="" cols="20" rows="5" class="form-control custom-form-control custom-textarea-control"></textarea>
                <span>Your comments and Questions</span>
            </label>
        </div>
        <div class="custom-form-control-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <button class="btn btn-md btn-submit" type="submit">Submit</button>
        </div>
    </div>

</form>
