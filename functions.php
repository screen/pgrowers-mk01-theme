<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue");
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.0.1', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */
require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/class-wp-bootstrap-navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');


/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

require_once('includes/wp_woocommerce_functions.php');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'pgrowers', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => '',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );

/* --------------------------------------------------------------
    ADD CUSTOM EDITOR STYLE
-------------------------------------------------------------- */
function pgrowers_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'pgrowers_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'pgrowers' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'pgrowers_widgets_init' );
function pgrowers_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'pgrowers' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'pgrowers' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 3, array(
        'name'          => __('Footer Section %d', 'pgrowers'),
        'id'            => 'sidebar_footer',
        'description'   => __('Footer Section', 'pgrowers'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'pgrowers' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'pgrowers' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    /* REGISTER THE CUSTOM WIDGETS */
    register_widget( 'pgrowers_custom_sidebar_banner' );
    register_widget( 'pgrowers_custom_mailchimp_form' );
    register_widget( 'pgrowers_custom_contact_form' );
    register_widget( 'pgrowers_custom_woocommerce_color_filter' );

}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_login_logo() {
    $version_remove = NULL;
    wp_register_style('wp-custom-login', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-custom-login');

}
add_action('login_head', 'custom_login_logo');

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'pgrowers' );
        echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'pgrowers' );
        echo '<a href="http://robertochoa.com.ve/?utm_source=footer_admin&utm_medium=link&utm_content=pgrowers" target="_blank">Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

//require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 450, 240, array('center', 'center'));
    add_image_size('single_img', 636, 297, true );
    add_image_size('banner_img', 9999, 470, array('center', 'center') );
    add_image_size('vertical_img', 350, 415, true );
}

function pgrowers_filter_glossary_callback  () {
    global $wpdb;
    $letter = $_POST['firstletter'] . '%';

    $filter_posts = $wpdb->get_results("SELECT post_title, post_content FROM $wpdb->posts WHERE post_type = 'glossary' AND post_status = 'publish' AND post_title LIKE '$letter'");
    foreach ($filter_posts as $posts) {
?>
<tr>
    <td colspan="2">
        <h3>
            <?php echo $posts->post_title; ?>
        </h3>
        <?php echo apply_filters('the_content', $posts->post_content); ?>
    </td>
</tr>
<?php
                                      }
    die();
}
add_action('wp_ajax_pgrowers_filter_glossary', 'pgrowers_filter_glossary_callback');
add_action('wp_ajax_nopriv_pgrowers_filter_glossary', 'pgrowers_filter_glossary_callback');
