<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php /* HOME SLIDER - TEMPLATE PART */ ?>
        <?php get_template_part('templates/home-block-slider'); ?>
        <?php /* HOME ABOUT - TEMPLATE PART */ ?>
        <?php get_template_part('templates/home-block-profile'); ?>
        <?php /* HOME SHOP CATEGORIES - TEMPLATE PART */ ?>
        <?php get_template_part('templates/home-block-shop-category'); ?>
        <?php /* HOME SHOP ROSE - TEMPLATE PART */ ?>
        <?php get_template_part('templates/home-block-rose-special'); ?>
        <?php /* HOME SHOP ROSE COLOR GUIDE - TEMPLATE PART */ ?>
        <div class="container-fluid">
            <div class="row">
                <div class="home-color-guide col-12" data-aos="fade-in" data-aos-duration="4000">
                    <h2>
                        <?php _e('Rose color guide', 'pgrowers'); ?>
                    </h2>
                    <p>Every Rose has is own meaning</p>
                    <a class="btn btn-md btn-orange" href="<?php echo home_url('rose-guide-color'); ?>" title="<?php _e('Discover them', 'pgrowers'); ?>">DISCOVER THEM</a>
                </div>
            </div>
        </div>
        <?php /* HOME FEATURED NEWS */ ?>
        <div class="container-fluid">
            <div class="row">
                <div class="home-featured-news col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <h2>
                        <?php _e('Featured News', 'pgrowers'); ?>
                    </h2>
                    <div class="w-100"></div>
                    <?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date'); ?>
                    <?php $query_news = new WP_Query($args); ?>
                    <?php if ($query_news->have_posts()) : ?>
                    <div class="container">
                        <div class="row align-items-start justify-content-center">
                            <div class="featured-news-item-container col-12 col-xl-10 col-lg-10 col-md-10 col-sm-12" data-aos="slide-up" data-aos-duration="4000">
                                <div class="container">
                                    <div class="row align-items-start justify-content-center">
                                        <div class="featured-news-item-wrapper col-12 col-xl-10 col-lg-10 col-md-10 col-sm-12">
                                            <div class="container">
                                                <div class="row no-gutters">
                                                    <?php while ($query_news->have_posts()) : $query_news->the_post(); ?>
                                                    <h3>
                                                        <?php the_title(); ?>
                                                    </h3>
                                                    <?php $categories = get_the_category(); ?>
                                                    <?php $array_cat = ''; ?>
                                                    <?php foreach ($categories as $category) { ?>
                                                    <?php $array_cat .= '<a href="'. get_term_link($category, 'category') .'" title="'. $category->name .'">'?>
                                                    <?php $array_cat .= '<span>'. $category->name .'</span>'; ?>
                                                    <?php $array_cat .= '</a>'; ?>
                                                    <?php } ?>
                                                    <h5>
                                                        <?php echo get_the_date('m.d.y'); ?> /
                                                        <?php echo $array_cat; ?>
                                                    </h5>
                                                    <div class="w-100"></div>
                                                    <div class="featured-news-item-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                        <?php the_post_thumbnail('full', array('class' => 'img-fluid img-featured-news')); ?>
                                                    </div>
                                                    <div class="featured-news-item-content col-xl-5 col-offset-xl-1 col-lg-5 offset-lg-1 col-md-12 col-sm-12 col-12">
                                                        <?php the_excerpt(); ?>
                                                        <a href="<?php the_permalink(); ?>">
                                                            <?php _e('Read More', 'pgrowers'); ?></a>
                                                    </div>
                                                    <?php endwhile; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <a class="btn btn-md btn-news" href="<?php echo home_url('/blog'); ?>">
                        <?php _e('SEE MORE NEWS', 'pgrowers'); ?></a>
                </div>
            </div>
        </div>
        <?php /* HOME FEATURED IN */ ?>
        <div class="container">
            <div class="row">
                <div class="home-featured-in col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade-in" data-aos-duration="4000">
                    <h2>
                        <?php _e('Featured In', 'pgrowers'); ?>
                    </h2>
                    <?php get_template_part('templates/home-block-featured-in');?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
