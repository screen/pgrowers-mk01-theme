<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="page-separator-bar col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
                    <div class="about-img-section col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <?php $about_people_images = get_custom_metabox_group(get_the_ID(), 'pg_giving_section1_group'); ?>
                        <?php if (!empty($about_people_images)) { ?>
                        <div class="page-img-overlayed page-img-overlayed2 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                            <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                            <div class="page-img-overlayed-item page-img-overlayed-item-<?php echo $i; ?>">
                                <?php echo wp_get_attachment_image( $feature['pg_giving_image_scalated_id'], 'vertical_img', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <div class="page-img-overlayed-slider owl-carousel owl-theme d-xl-none d-lg-none d-md-block d-sm-block d-block">
                            <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                            <div class="item">
                                <?php echo wp_get_attachment_image( $feature['pg_giving_image_scalated_id'], 'vertical_img', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="about-text-section col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php $about_double_desc = get_custom_metabox_group(get_the_ID(), 'pg_giving_section2_double_desc_group'); ?>
            <?php if (!empty($about_double_desc)) { ?>
            <div class="container">
                <div class="row">
                    <div class="about-page-title-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo get_post_meta(get_the_ID(), 'pg_giving_section2_title', true); ?>
                    </div>
                    <div class="page-text-section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row align-items-top justify-content-center">
                                <?php foreach ($about_double_desc as $desc_item) { ?>
                                <div class="page-text-section col-xl-5 col-lg-5 col-md-5 col-sm-6 col-12">
                                    <?php echo apply_filters( 'the_content', $desc_item['pg_giving_section2_desc'] ); ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php $about_slider_images = get_custom_metabox_group(get_the_ID(), 'pg_giving_section3_group'); ?>
                    <?php if (!empty($about_slider_images)) { ?>
                    <div class="page-section-slider-container col-12">
                        <div class="page-section-slider owl-carousel owl-theme">
                            <?php foreach ( $about_slider_images as $feature ) {?>
                            <div class="item">
                                <?php echo wp_get_attachment_image( $feature['pg_giving_image_scalated_id'], 'full', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
