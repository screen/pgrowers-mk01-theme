<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="footer-height-controller col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
            <div class="container">
                <div class="row">
                    <div class="footer-item col-xl col-lg col-md-12 col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                        <ul id="sidebar-footer">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                        <?php endif; ?>
                        <div class="footer-social">
                            <hr>
                            <h6><?php _e('Follow Us', 'pgrowers'); ?></h6>
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-instagram"></i>
                        </div>
                    </div>
                    <div class="footer-item col-xl col-lg col-md-12 col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                        <ul id="sidebar-footer">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="footer-item col-xl col-lg col-md-12 col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                        <ul id="sidebar-footer">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="w-100"></div>
                    <div class="footer-copyright col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h6><?php _e('Copyright &copy; 2018 Passion Growers', 'pgrowers'); ?></h6>
                        <h5><?php _e('Developed by SMG | Digital Marketing Agency', 'pgrowers'); ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
