<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php get_template_part('templates/block-main-banner'); ?>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php the_content(); ?>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="about-img-section col-xl-6 col-lg-6 col-md-6 col-sm-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $about_people_images = get_custom_metabox_group(get_the_ID(), 'pg_about_people_img_group'); ?>
                        <?php if (!empty($about_people_images)) { ?>
                        <div class="page-img-overlayed">
                            <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                            <div class="page-img-overlayed-item page-img-overlayed-item-<?php echo $i; ?>">
                                <?php echo wp_get_attachment_image( $feature['pg_about_people_scalonated_img_id'], 'medium', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="about-text-section col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'pg_about_main_values_content', true); ?>
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $content = get_post_meta(get_the_ID(), 'pg_about_core_values_content', true); ?>
                <?php echo apply_filters('the_content', $content); ?>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="about-core-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $banner_id = get_post_meta(get_the_ID(), 'pg_about_core_items_background_id', true); ?>
                        <?php echo wp_get_attachment_image( $banner_id, 'full', '', array('class' => 'img-fluid') ); ?>
                        <div class="about-core-floating-section col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                            <?php $about_icons_array = get_custom_metabox_group(get_the_ID(), 'pg_about_core_values_item_group'); ?>
                            <?php if (!empty($about_icons_array)) { ?>
                            <div class="container">
                                <div class="row">
                                    <?php foreach ($about_icons_array as $about_icon) { ?>
                                    <div class="core-values-icon col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="row align-items-center">
                                            <div class="about-core-values-img col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                                <img src="<?php echo esc_url($about_icon['pg_about_core_values_item_img']); ?>" alt="" class="img-fluid" />
                                            </div>
                                            <div class="about-core-values-content col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                                <?php echo $about_icon['pg_about_core_values_item_text']; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $content = get_post_meta(get_the_ID(), 'pg_about_enviroment_content', true); ?>
                <?php echo apply_filters('the_content', $content); ?>
            </div>
        </section>
        <section class="page-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">

                    <div class="about-text-section col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'pg_about_enviroment_description', true); ?>
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                    <div class="about-img-section col-xl-6 col-lg-6 col-md-6 col-sm-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $about_people_images = get_custom_metabox_group(get_the_ID(), 'pg_about_enviroment_group'); ?>
                        <?php if (!empty($about_people_images)) { ?>
                        <div class="page-img-overlayed">
                            <?php $i=1; foreach ( $about_people_images as $feature ) {?>
                            <div class="page-img-overlayed-item page-img-overlayed-item-<?php echo $i; ?>">
                                <?php echo wp_get_attachment_image( $feature['pg_about_enviroment_scalonated_img_id'], 'medium', false, array('class' => 'img-fluid') ); ?>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
